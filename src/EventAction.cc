#include "EventAction.hh"
#include "EventInformation.hh"
#include <CLHEP/Units/SystemOfUnits.h>   // for GeV
#include <CLHEP/Vector/ThreeVector.h>    // for Hep3Vector
#include <stddef.h>                      // for size_t
#include <G4Exception.hh>                // for G4Exception, G4ExceptionDesc...
#include <G4ExceptionSeverity.hh>        // for FatalException
#include <G4GenericAnalysisManager.hh>   // for G4GenericAnalysisManager
#include <G4SystemOfUnits.hh>            // for GeV
#include <G4THitsCollection.hh>          // for G4THitsCollection
#include <G4ThreeVector.hh>              // for G4ThreeVector
#include <G4Timer.hh>                    // for G4Timer
#include <G4UserEventAction.hh>          // for G4UserEventAction
#include <algorithm>                     // for max
#include <ostream>                       // for basic_ostream::operator<<
#include "G4AnalysisManager.hh"          // for G4AnalysisManager
#include "G4Event.hh"                    // for G4Event
#include "G4EventManager.hh"             // for G4EventManager

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::EventAction()
  : G4UserEventAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::~EventAction() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EventAction::BeginOfEventAction(const G4Event*) {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EventAction::EndOfEventAction(const G4Event* aEvent)
{
  // Get analysis manager
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

  EventInformation* info = dynamic_cast<EventInformation*> (aEvent->GetUserInformation());

  for (int bin = 0; bin < 4; bin++)
  {
    G4int electrons = info->GetElectrons(bin);
    G4int gammas = info->GetGammas(bin);
    G4int muons = info->GetMuons(bin);
    G4int pi0s = info->GetPi0s(bin);
    G4int charged_pions = info->GetChargedPions(bin);
    G4int protons = info->GetProtons(bin);
    G4int neutrons = info->GetNeutrons(bin);
    G4int neutrinos = info->GetNeutrinos(bin);
    G4int others = info->GetOthers(bin);
    G4int all = electrons + gammas + muons + pi0s + charged_pions + protons + neutrons + neutrinos + others;

    analysisManager->FillH1(0+bin*10, all);
    analysisManager->FillH1(1+bin*10, electrons);
    analysisManager->FillH1(2+bin*10, gammas);
    analysisManager->FillH1(3+bin*10, muons);
    analysisManager->FillH1(4+bin*10, pi0s);
    analysisManager->FillH1(5+bin*10, charged_pions);
    analysisManager->FillH1(6+bin*10, protons);
    analysisManager->FillH1(7+bin*10, neutrons);
    analysisManager->FillH1(8+bin*10, neutrinos);
    analysisManager->FillH1(9+bin*10, others);
  }
}
