#include "PrimaryGeneratorAction.hh"
#include <CLHEP/Units/SystemOfUnits.h>       // for GeV
#include <G4String.hh>                       // for G4String
#include <G4ThreeVector.hh>                  // for G4ThreeVector
#include <G4Types.hh>                        // for G4int
#include <G4VUserPrimaryGeneratorAction.hh>  // for G4VUserPrimaryGeneratorA...
#include <string>                            // for basic_string
#include "G4Event.hh"                        // for G4Event
#include "G4ParticleGun.hh"                  // for G4ParticleGun
#include "G4ParticleTable.hh"                // for G4ParticleTable
#include "G4SystemOfUnits.hh"                // for GeV
#include "EventInformation.hh"          // for EventInformation
#include "HepMCG4Interface.hh"
class G4ParticleDefinition;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::PrimaryGeneratorAction()
  : G4VUserPrimaryGeneratorAction()
{
  SetFileName(fFileName);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::~PrimaryGeneratorAction() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGeneratorAction::SetFileName(G4String aName) {
  fFileName = aName;
  HepMC3::ReaderAscii reader(fFileName);
  
  while(!reader.failed()) {
    std::unique_ptr<HepMC3::GenEvent> event(new HepMC3::GenEvent);
    if (!reader.read_event(*event) || reader.failed())
      break;

    events.push_back(std::move(event));
  }
  if(events.size() == 0) {
    G4Exception("PrimaryGeneratorAction::SetFileName", "No events",
		FatalException, "Failure to read events from given file.");
  }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* aEvent)
{
  size_t eventId = aEvent->GetEventID();
  if (eventId >= events.size())
    G4Exception("PrimaryGeneratorAction::GeneratePrimaries", "No more events",
		FatalException, "Requested more events than available in the file.");
  HepMC2G4(events[eventId].get(), aEvent);
  aEvent->SetUserInformation(new EventInformation());
}
