#include "ActionInitialisation.hh"
#include <G4VUserActionInitialization.hh>  // for G4VUserActionInitialization
#include "PrimaryGeneratorAction.hh"  // for PrimaryGeneratorAction
#include "EventAction.hh"
#include "RunAction.hh"

ActionInitialisation::ActionInitialisation()
  : G4VUserActionInitialization()
{}

ActionInitialisation::~ActionInitialisation() {}

void ActionInitialisation::BuildForMaster() const {
  SetUserAction(new RunAction());
}

void ActionInitialisation::Build() const
{
  SetUserAction(new PrimaryGeneratorAction());
  SetUserAction(new EventAction());
  SetUserAction(new RunAction());
}
