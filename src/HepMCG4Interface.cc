#include "HepMCG4Interface.hh"

#include "G4RunManager.hh"
#include "G4LorentzVector.hh"
#include "G4Event.hh"
#include "G4PrimaryParticle.hh"
#include "G4PrimaryVertex.hh"
#include "G4TransportationManager.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "HepMC3/GenVertex.h"
#include "HepMC3/GenParticle.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4bool CheckVertexInsideWorld (const G4ThreeVector& aPos)
{
  G4Navigator* navigator= G4TransportationManager::GetTransportationManager()
                                                 -> GetNavigatorForTracking();

  G4VPhysicalVolume* world= navigator->GetWorldVolume();
  G4VSolid* solid= world->GetLogicalVolume()->GetSolid();
  EInside qinside= solid->Inside(aPos);

  if( qinside != kInside) return false;
  else return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void HepMC2G4(const HepMC3::GenEvent* aHepMCEvent, G4Event* G4Event)
{
  auto momentumUnit = aHepMCEvent->momentum_unit();
  auto lengthUnit = aHepMCEvent->length_unit();
  for (HepMC3::ConstGenVertexPtr vertex : aHepMCEvent->vertices()) {
    // real vertex?
    G4bool real=false;
    double totalE = 0;
    for (HepMC3::ConstGenParticlePtr particle : vertex->particles_out()) {
      if (!particle->end_vertex() && particle->status()==1) {
        real=true;
	totalE += HepMC2G4Momentum(particle->momentum().e(), momentumUnit);
        break;
      }
    }
    if (!real || totalE == 0) continue;

    // check world boundary
    HepMC3::FourVector pos = vertex->position();
    G4LorentzVector xvtx(
			 HepMC2G4Length(pos.x(), lengthUnit),
			 HepMC2G4Length(pos.y(), lengthUnit),
			 HepMC2G4Length(pos.z(), lengthUnit),
			 HepMC2G4Length(pos.t(), lengthUnit) / c_light);

    if (! CheckVertexInsideWorld(xvtx.vect()*mm)) continue;

    // create G4PrimaryVertex and associated G4PrimaryParticles
    G4PrimaryVertex* g4vtx=
      new G4PrimaryVertex(xvtx.x(), xvtx.y(), xvtx.z(), xvtx.t());

    for (HepMC3::ConstGenParticlePtr particle : vertex->particles_out()) {

      if( particle->status() != 1 ) continue;

      G4int pdgcode = particle->pdg_id();
      HepMC3::FourVector mom = particle->momentum();
      G4LorentzVector p(HepMC2G4Momentum(mom.px(), momentumUnit),
			HepMC2G4Momentum(mom.py(), momentumUnit),
			HepMC2G4Momentum(mom.pz(), momentumUnit),
			HepMC2G4Momentum(mom.e(), momentumUnit));
      G4PrimaryParticle* g4prim= new G4PrimaryParticle(pdgcode, p.x(), p.y(), p.z());

      g4vtx->SetPrimary(g4prim);
    }
    G4Event->AddPrimaryVertex(g4vtx);
  }
}
