#include <G4Exception.hh>                // for G4Exception
#include <G4ExceptionSeverity.hh>        // for FatalErrorInArgument
#include <G4RunManager.hh>               // for G4RunManager
#include <G4String.hh>                   // for G4String
#include <G4VisManager.hh>               // for G4VisManager
#include <G4ios.hh>                      // for G4cout, G4endl
#include <sstream>                       // for char_traits, operator<<, bas...
#include <string>                        // for allocator, operator+, operat...
#include "FTFP_BERT.hh"                  // for FTFP_BERT
#include "G4EmParameters.hh"             // for G4EmParameters
#include "G4FastSimulationPhysics.hh"    // for G4FastSimulationPhysics
#include "G4RunManagerFactory.hh"        // for G4RunManagerFactory, G4RunMa...
#include "G4Types.hh"                    // for G4bool, G4int
#include "G4UIExecutive.hh"              // for G4UIExecutive
#include "G4UImanager.hh"                // for G4UImanager
#include "G4VisExecutive.hh"             // for G4VisExecutive
#include "ActionInitialisation.hh"  // for ActionInitialisation
#include "DetectorConstruction.hh"  // for DetectorConstruction

int main(int argc, char** argv)
{
  // Macro name from arguments
  G4String batchMacroName;
  G4bool useInteractiveMode = false;
  G4int numOfThreadsOrTasks = 8;
  G4int runManagerTypeInt = 0;
  G4double maxEta = 1.;
  G4RunManagerType runManagerType = G4RunManagerType::Serial;
  G4String helpMsg(
    "Usage: " + G4String(argv[0]) +
    " [option(s)] \n You need to specify the mode and the macro file.\nOptions:"
    "\n\t-h\t\tdisplay this help message\n\t-m MACRO\ttriggers a batch mode "
     "executing MACRO\n\t-i\t\truns interactive mode, use it together with <-m vis*mac> macros"
    "\n\t-r\t\trun manager type (0=serial,1=MT,2=tasking)"
    "\n\t-t\t\tnumber of threads for MT mode or tasks for tasking mode"
    "\n\t--eta\t\tspecify eta coverage for this check."
    );
  if(argc < 2 ) {
    G4Exception("main", "No arguments", FatalErrorInArgument,
                ("No arguments passed to " + G4String(argv[0]) + "\n" + helpMsg)
                .c_str());
  }
  for(G4int i = 1; i < argc; ++i)
  {
    G4String argument(argv[i]);
    if(argument == "-h" || argument == "--help")
    {
      G4cout << helpMsg << G4endl;
      return 0;
    }
    else if(argument == "-m")
    {
      batchMacroName     = G4String(argv[i + 1]);
      ++i;
    }
    else if(argument == "-i")
    {
      useInteractiveMode = true;
    }
    else if(argument == "-r")
    {
      G4int tmp = atoi(argv[i + 1]);
      ++i;
      switch (tmp) {
      case 0:
	runManagerTypeInt = tmp;
	runManagerType = G4RunManagerType::Serial;
	break;
      case 1:
	runManagerTypeInt = tmp;
	runManagerType = G4RunManagerType::MTOnly;
	break;
      case 2:
	runManagerTypeInt = tmp;
	runManagerType = G4RunManagerType::Tasking;
	break;
      default:
	G4Exception("main", "Wrong Run Manager type", FatalErrorInArgument,
                "Choose 0 (serial, default), 1 (MT), 2 (tasking)");
	break;
      }
    }
    else if(argument == "-t")
    {
      numOfThreadsOrTasks = atoi(argv[i + 1]);
      ++i;
    }
    else if(argument == "--eta")
    {
      maxEta = std::stod(argv[i + 1]);
      ++i;
    }
    else
    {
      G4Exception("main", "Unknown argument", FatalErrorInArgument,
                  ("Unknown argument passed to " + G4String(argv[0]) + " : " +
                   argument + "\n" + helpMsg)
                    .c_str());
    }
  }

  //choose the Random engine
  CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine());
  //set random seed with system time
  G4long seed = time(NULL);
  CLHEP::HepRandom::setTheSeed(seed);

  // Instantiate G4UIExecutive if interactive mode
  G4UIExecutive* ui = nullptr;
  
  if(useInteractiveMode)
  {
    ui = new G4UIExecutive(argc, argv);
    runManagerType = G4RunManagerType::Serial;
  }

  // Initialization of default Run manager
  if(runManagerTypeInt == 2) {
    setenv("G4FORCE_EVENTS_PER_TASK",(std::to_string(numOfThreadsOrTasks)).c_str(),1);
  }
  auto* runManager =
    G4RunManagerFactory::CreateRunManager(runManagerType);
  if(runManagerTypeInt == 1)
    runManager->SetNumberOfThreads(numOfThreadsOrTasks);

  // Detector geometry:
  auto detector = new DetectorConstruction(maxEta);
  runManager->SetUserInitialization(detector);

  // Physics list
  auto physicsList = new FTFP_BERT();
  // Add fast simulation physics
  auto fastSimulationPhysics = new G4FastSimulationPhysics();
  fastSimulationPhysics->BeVerbose();
  physicsList->RegisterPhysics(fastSimulationPhysics);
  // Register it to all known particles
  std::vector<G4String> particle_names = {
    "B+", "B-", "B0", "Bc+", "Bc-", "Bs0", "D+", "D-", "D0", "Ds+", "Ds-", "GenericIon", "He3", "J/psi", "N(1440)+", "N(1440)0", "N(1520)+", "N(1520)0", "N(1535)+", "N(1535)0", "N(1650)+", "N(1650)0", "N(1675)+", "N(1675)0", "N(1680)+", "N(1680)0", "N(1700)+", "N(1700)0", "N(1710)+", "N(1710)0", "N(1720)+", "N(1720)0", "N(1900)+", "N(1900)0", "N(1990)+", "N(1990)0", "N(2090)+", "N(2090)0", "N(2190)+", "N(2190)0", "N(2220)+", "N(2220)0", "N(2250)+", "N(2250)0", "Upsilon", "a0(1450)+", "a0(1450)-", "a0(1450)0", "a0(980)+", "a0(980)-", "a0(980)0", "a1(1260)+", "a1(1260)-", "a1(1260)0", "a2(1320)+", "a2(1320)-", "a2(1320)0", "alpha", "anti_B0", "anti_Bs0", "anti_D0", "anti_He3", "anti_N(1440)+", "anti_N(1440)0", "anti_N(1520)+", "anti_N(1520)0", "anti_N(1535)+", "anti_N(1535)0", "anti_N(1650)+", "anti_N(1650)0", "anti_N(1675)+", "anti_N(1675)0", "anti_N(1680)+", "anti_N(1680)0", "anti_N(1700)+", "anti_N(1700)0", "anti_N(1710)+", "anti_N(1710)0", "anti_N(1720)+", "anti_N(1720)0", "anti_N(1900)+", "anti_N(1900)0", "anti_N(1990)+", "anti_N(1990)0", "anti_N(2090)+", "anti_N(2090)0", "anti_N(2190)+", "anti_N(2190)0", "anti_N(2220)+", "anti_N(2220)0", "anti_N(2250)+", "anti_N(2250)0", "anti_alpha", "anti_b_quark", "anti_bb1_diquark", "anti_bc0_diquark", "anti_bc1_diquark", "anti_bd0_diquark", "anti_bd1_diquark", "anti_bs0_diquark", "anti_bs1_diquark", "anti_bu0_diquark", "anti_bu1_diquark", "anti_c_quark", "anti_cc1_diquark", "anti_cd0_diquark", "anti_cd1_diquark", "anti_cs0_diquark", "anti_cs1_diquark", "anti_cu0_diquark", "anti_cu1_diquark", "anti_d_quark", "anti_dd1_diquark", "anti_delta(1600)+", "anti_delta(1600)++", "anti_delta(1600)-", "anti_delta(1600)0", "anti_delta(1620)+", "anti_delta(1620)++", "anti_delta(1620)-", "anti_delta(1620)0", "anti_delta(1700)+", "anti_delta(1700)++", "anti_delta(1700)-", "anti_delta(1700)0", "anti_delta(1900)+", "anti_delta(1900)++", "anti_delta(1900)-", "anti_delta(1900)0", "anti_delta(1905)+", "anti_delta(1905)++", "anti_delta(1905)-", "anti_delta(1905)0", "anti_delta(1910)+", "anti_delta(1910)++", "anti_delta(1910)-", "anti_delta(1910)0", "anti_delta(1920)+", "anti_delta(1920)++", "anti_delta(1920)-", "anti_delta(1920)0", "anti_delta(1930)+", "anti_delta(1930)++", "anti_delta(1930)-", "anti_delta(1930)0", "anti_delta(1950)+", "anti_delta(1950)++", "anti_delta(1950)-", "anti_delta(1950)0", "anti_delta+", "anti_delta++", "anti_delta-", "anti_delta0", "anti_deuteron", "anti_doublehyperH4", "anti_doublehyperdoubleneutron", "anti_hyperH4", "anti_hyperHe5", "anti_hyperalpha", "anti_hypertriton", "anti_k(1460)0", "anti_k0_star(1430)0", "anti_k1(1270)0", "anti_k1(1400)0", "anti_k2(1770)0", "anti_k2_star(1430)0", "anti_k2_star(1980)0", "anti_k3_star(1780)0", "anti_k_star(1410)0", "anti_k_star(1680)0", "anti_k_star0", "anti_kaon0", "anti_lambda", "anti_lambda(1405)", "anti_lambda(1520)", "anti_lambda(1600)", "anti_lambda(1670)", "anti_lambda(1690)", "anti_lambda(1800)", "anti_lambda(1810)", "anti_lambda(1820)", "anti_lambda(1830)", "anti_lambda(1890)", "anti_lambda(2100)", "anti_lambda(2110)", "anti_lambda_b", "anti_lambda_c+", "anti_neutron", "anti_nu_e", "anti_nu_mu", "anti_nu_tau", "anti_omega-", "anti_omega_b-", "anti_omega_c0", "anti_proton", "anti_s_quark", "anti_sd0_diquark", "anti_sd1_diquark", "anti_sigma(1385)+", "anti_sigma(1385)-", "anti_sigma(1385)0", "anti_sigma(1660)+", "anti_sigma(1660)-", "anti_sigma(1660)0", "anti_sigma(1670)+", "anti_sigma(1670)-", "anti_sigma(1670)0", "anti_sigma(1750)+", "anti_sigma(1750)-", "anti_sigma(1750)0", "anti_sigma(1775)+", "anti_sigma(1775)-", "anti_sigma(1775)0", "anti_sigma(1915)+", "anti_sigma(1915)-", "anti_sigma(1915)0", "anti_sigma(1940)+", "anti_sigma(1940)-", "anti_sigma(1940)0", "anti_sigma(2030)+", "anti_sigma(2030)-", "anti_sigma(2030)0", "anti_sigma+", "anti_sigma-", "anti_sigma0", "anti_sigma_b+", "anti_sigma_b-", "anti_sigma_b0", "anti_sigma_c+", "anti_sigma_c++", "anti_sigma_c0", "anti_ss1_diquark", "anti_su0_diquark", "anti_su1_diquark", "anti_t_quark", "anti_triton", "anti_u_quark", "anti_ud0_diquark", "anti_ud1_diquark", "anti_uu1_diquark", "anti_xi(1530)-", "anti_xi(1530)0", "anti_xi(1690)-", "anti_xi(1690)0", "anti_xi(1820)-", "anti_xi(1820)0", "anti_xi(1950)-", "anti_xi(1950)0", "anti_xi(2030)-", "anti_xi(2030)0", "anti_xi-", "anti_xi0", "anti_xi_b-", "anti_xi_b0", "anti_xi_c+", "anti_xi_c0", "b1(1235)+", "b1(1235)-", "b1(1235)0", "b_quark", "bb1_diquark", "bc0_diquark", "bc1_diquark", "bd0_diquark", "bd1_diquark", "bs0_diquark", "bs1_diquark", "bu0_diquark", "bu1_diquark", "c_quark", "cc1_diquark", "cd0_diquark", "cd1_diquark", "chargedgeantino", "cs0_diquark", "cs1_diquark", "cu0_diquark", "cu1_diquark", "d_quark", "dd1_diquark", "delta(1600)+", "delta(1600)++", "delta(1600)-", "delta(1600)0", "delta(1620)+", "delta(1620)++", "delta(1620)-", "delta(1620)0", "delta(1700)+", "delta(1700)++", "delta(1700)-", "delta(1700)0", "delta(1900)+", "delta(1900)++", "delta(1900)-", "delta(1900)0", "delta(1905)+", "delta(1905)++", "delta(1905)-", "delta(1905)0", "delta(1910)+", "delta(1910)++", "delta(1910)-", "delta(1910)0", "delta(1920)+", "delta(1920)++", "delta(1920)-", "delta(1920)0", "delta(1930)+", "delta(1930)++", "delta(1930)-", "delta(1930)0", "delta(1950)+", "delta(1950)++", "delta(1950)-", "delta(1950)0", "delta+", "delta++", "delta-", "delta0", "deuteron", "doublehyperH4", "doublehyperdoubleneutron", "e+", "e-", "eta", "eta(1295)", "eta(1405)", "eta(1475)", "eta2(1645)", "eta2(1870)", "eta_prime", "etac", "f0(1370)", "f0(1500)", "f0(1710)", "f0(500)", "f0(980)", "f1(1285)", "f1(1420)", "f2(1270)", "f2(1810)", "f2(2010)", "f2_prime(1525)", "gamma", "geantino", "gluon", "h1(1170)", "h1(1380)", "hyperH4", "hyperHe5", "hyperalpha", "hypertriton", "k(1460)+", "k(1460)-", "k(1460)0", "k0_star(1430)+", "k0_star(1430)-", "k0_star(1430)0", "k1(1270)+", "k1(1270)-", "k1(1270)0", "k1(1400)+", "k1(1400)-", "k1(1400)0", "k2(1770)+", "k2(1770)-", "k2(1770)0", "k2_star(1430)+", "k2_star(1430)-", "k2_star(1430)0", "k2_star(1980)+", "k2_star(1980)-", "k2_star(1980)0", "k3_star(1780)+", "k3_star(1780)-", "k3_star(1780)0", "k_star(1410)+", "k_star(1410)-", "k_star(1410)0", "k_star(1680)+", "k_star(1680)-", "k_star(1680)0", "k_star+", "k_star-", "k_star0", "kaon+", "kaon-", "kaon0", "kaon0L", "kaon0S", "lambda", "lambda(1405)", "lambda(1520)", "lambda(1600)", "lambda(1670)", "lambda(1690)", "lambda(1800)", "lambda(1810)", "lambda(1820)", "lambda(1830)", "lambda(1890)", "lambda(2100)", "lambda(2110)", "lambda_b", "lambda_c+", "mu+", "mu-", "neutron", "nu_e", "nu_mu", "nu_tau", "omega", "omega(1420)", "omega(1650)", "omega-", "omega3(1670)", "omega_b-", "omega_c0", "opticalphoton", "phi", "phi(1680)", "phi3(1850)", "pi(1300)+", "pi(1300)-", "pi(1300)0", "pi+", "pi-", "pi0", "pi2(1670)+", "pi2(1670)-", "pi2(1670)0", "proton", "rho(1450)+", "rho(1450)-", "rho(1450)0", "rho(1700)+", "rho(1700)-", "rho(1700)0", "rho+", "rho-", "rho0", "rho3(1690)+", "rho3(1690)-", "rho3(1690)0", "s_quark", "sd0_diquark", "sd1_diquark", "sigma(1385)+", "sigma(1385)-", "sigma(1385)0", "sigma(1660)+", "sigma(1660)-", "sigma(1660)0", "sigma(1670)+", "sigma(1670)-", "sigma(1670)0", "sigma(1750)+", "sigma(1750)-", "sigma(1750)0", "sigma(1775)+", "sigma(1775)-", "sigma(1775)0", "sigma(1915)+", "sigma(1915)-", "sigma(1915)0", "sigma(1940)+", "sigma(1940)-", "sigma(1940)0", "sigma(2030)+", "sigma(2030)-", "sigma(2030)0", "sigma+", "sigma-", "sigma0", "sigma_b+", "sigma_b-", "sigma_b0", "sigma_c+", "sigma_c++", "sigma_c0", "ss1_diquark", "su0_diquark", "su1_diquark", "t_quark", "tau+", "tau-", "triton", "u_quark", "ud0_diquark", "ud1_diquark", "uu1_diquark", "xi(1530)-", "xi(1530)0", "xi(1690)-", "xi(1690)0", "xi(1820)-", "xi(1820)0", "xi(1950)-", "xi(1950)0", "xi(2030)-", "xi(2030)0", "xi-", "xi0", "xi_b-", "xi_b0", "xi_c+", "xi_c0",  };
  for (auto name: particle_names  )
  {
    fastSimulationPhysics->ActivateFastSimulation(name);
   }
  // reduce verbosity of physics lists
  G4EmParameters::Instance()->SetVerbose(0);
  runManager->SetUserInitialization(physicsList);

  //-------------------------------
  // UserAction classes
  //-------------------------------
  runManager->SetUserInitialization(new ActionInitialisation());

  //----------------
  // Visualization:
  //----------------
  G4cout << "Instantiating Visualization Manager......." << G4endl;
  G4VisManager* visManager = new G4VisExecutive;
  visManager->Initialize();
  G4UImanager* UImanager = G4UImanager::GetUIpointer();

  UImanager->ApplyCommand("/process/had/verbose 0");
  runManager->Initialize();

  if(useInteractiveMode)
  {
    if(batchMacroName.empty())
    {
      G4Exception("main", "Unknown macro name", FatalErrorInArgument,
                  ("No macro name passed to " + G4String(argv[0]))
                    .c_str());
    }
    G4String command = "/control/execute ";
    UImanager->ApplyCommand(command + batchMacroName);
    ui->SessionStart();
    delete ui;
  }
  else
  {
    G4String command = "/control/execute ";
    UImanager->ApplyCommand(command + batchMacroName);
  }

  // Free the store: user actions, physics_list and detector_description are
  //                 owned and deleted by the run manager, so they should not
  //                 be deleted in the main() program !

  delete visManager;
  delete runManager;

  return 0;
}
