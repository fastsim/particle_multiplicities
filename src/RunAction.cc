#include "RunAction.hh"
#include <G4GenericAnalysisManager.hh>   // for G4GenericAnalysisManager
#include <G4ThreeVector.hh>              // for G4ThreeVector
#include <G4Types.hh>                    // for G4int, G4double
#include <G4UserRunAction.hh>            // for G4UserRunAction
#include "G4AnalysisManager.hh"          // for G4AnalysisManager
#include "DetectorConstruction.hh"  // for DetectorConstruction
#include "EventAction.hh"           // for EventAction

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::RunAction()
  : G4UserRunAction()
{
  // Create analysis manager
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  analysisManager->SetDefaultFileType("root");

  // Default filename, can be overriden with /analysis/setFileName
  analysisManager->SetFileName("Output");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::~RunAction() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void RunAction::BeginOfRunAction(const G4Run*)
{
  // Get analysis manager
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

  // Create directories
  analysisManager->SetVerboseLevel(0);

  // Creating control histograms
  analysisManager->CreateH2("energyEtaDistributionAllParticles", "EnergyEtaDistribution of all particles;E_{MC} (GeV);eta;Entries", 4048, 0, 512, 901, -9, 9);
  analysisManager->CreateH2("energyEtaDistributionElectronPositron", "EnergyEtaDistribution of e+e-;E_{MC} (GeV);eta;Entries", 4048, 0, 512, 901, -9, 9);
  analysisManager->CreateH2("energyEtaDistributionGamma", "EnergyEtaDistribution of gammas;E_{MC} (GeV);eta;Entries", 4048, 0, 512, 901, -9, 9);
  analysisManager->CreateH2("energyEtaDistributionMuons", "EnergyEtaDistribution of muons;E_{MC} (GeV);eta;Entries", 4048, 0, 512, 901, -9, 9);
  analysisManager->CreateH2("energyEtaDistributionPi0s", "EnergyEtaDistribution of pi0s;E_{MC} (GeV);eta;Entries", 4048, 0, 512, 901, -9, 9);
  analysisManager->CreateH2("energyEtaDistributionChargedPions", "EnergyEtaDistribution of charged pions;E_{MC} (GeV);eta;Entries", 4048, 0, 512, 901, -9, 9);
  analysisManager->CreateH2("energyEtaDistributionProtons", "EnergyEtaDistribution of protons;E_{MC} (GeV);eta;Entries", 4048, 0, 512, 901, -9, 9);
  analysisManager->CreateH2("energyEtaDistributionNeutrons", "EnergyEtaDistribution of neutrons;E_{MC} (GeV);eta;Entries", 4048, 0, 512, 901, -9, 9);
  analysisManager->CreateH2("energyEtaDistributionNeutrinos", "EnergyEtaDistribution of neutrinos;E_{MC} (GeV);eta;Entries", 4048, 0, 512, 901, -9, 9);
  analysisManager->CreateH2("energyEtaDistributionOthers", "EnergyEtaDistribution of the rest particles;E_{MC} (GeV);eta;Entries", 4048, 0, 512, 901, -9, 9);

  analysisManager->CreateH1("meanCountAllParticlesE100MeV", "Mean number of all particles with E>100MeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountElectronPositronE100MeV", "Mean number of e+e- with E>100MeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountGammaE100MeV", "Mean number of gammas with E>100MeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountMuonsE100MeV", "Mean number of muons with E>100MeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountPi0sE100MeV", "Mean number of pi0s with E>100MeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountChargedPionsE100MeV", "Mean number of charged pions with E>100MeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountProtonsE100MeV", "Mean number of protons with E>100MeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountNeutronsE100MeV", "Mean number of neutrons with E>100MeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountNeutrinosE100MeV", "Mean number of neutrinos with E>100MeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountOthersE100MeV", "Mean number of the rest particles with E>100MeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  
  analysisManager->CreateH1("meanCountAllParticlesE1GeV", "Mean number of all particles with E>1GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountElectronPositronE1GeV", "Mean number of e+e- with E>1GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountGammaE1GeV", "Mean number of gammas with E>1GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountMuonsE1GeV", "Mean number of muons with E>1GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountPi0sE1GeV", "Mean number of pi0s with E>1GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountChargedPionsE1GeV", "Mean number of charged pions with E>1GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountProtonsE1GeV", "Mean number of protons with E>1GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountNeutronsE1GeV", "Mean number of neutrons with E>1GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountNeutrinosE1GeV", "Mean number of neutrinos with E>1GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountOthersE1GeV", "Mean number of the rest particles with E>1GeV;mean number per event;Entries", 1024, -0.5, 1023.5);

  analysisManager->CreateH1("meanCountAllParticlesE10GeV", "Mean number of all particles with E>10GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountElectronPositronE10GeV", "Mean number of e+e- with E>10GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountGammaE10GeV", "Mean number of gammas with E>10GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountMuonsE10GeV", "Mean number of muons with E>10GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountPi0sE10GeV", "Mean number of pi0s with E>10GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountChargedPionsE10GeV", "Mean number of charged pions with E>10GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountProtonsE10GeV", "Mean number of protons with E>10GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountNeutronsE10GeV", "Mean number of neutrons with E>10GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountNeutrinosE10GeV", "Mean number of neutrinos with E>10GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountOthersE10GeV", "Mean number of the rest particles with E>10GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  
  analysisManager->CreateH1("meanCountAllParticlesE100GeV", "Mean number of all particles with E>100GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountElectronPositronE100GeV", "Mean number of e+e- with E>100GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountGammaE100GeV", "Mean number of gammas with E>100GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountMuonsE100GeV", "Mean number of muons with E>100GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountPi0sE100GeV", "Mean number of pi0s with E>100GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountChargedPionsE100GeV", "Mean number of charged pions with E>100GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountProtonsE100GeV", "Mean number of protons with E>100GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountNeutronsE100GeV", "Mean number of neutrons with E>100GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountNeutrinosE100GeV", "Mean number of neutrinos with E>100GeV;mean number per event;Entries", 1024, -0.5, 1023.5);
  analysisManager->CreateH1("meanCountOthersE100GeV", "Mean number of the rest particles with E>100GeV;mean number per event;Entries", 1024, -0.5, 1023.5);

  analysisManager->OpenFile();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::EndOfRunAction(const G4Run*)
{
  auto analysisManager = G4AnalysisManager::Instance();
  analysisManager->Write();
  analysisManager->CloseFile();
}
