#include "RegisterParticlesModel.hh"
#include <G4FastTrack.hh>              // for G4FastTrack
#include <G4Track.hh>                  // for G4Track
#include <G4VFastSimulationModel.hh>   // for G4VFastSimulationModel
#include <G4VUserEventInformation.hh>  // for G4VUserEventInformation
#include "G4Event.hh"                  // for G4Event
#include "G4EventManager.hh"           // for G4EventManager
#include "EventInformation.hh"    // for EventInformation
class G4FastStep;
class G4ParticleDefinition;
class G4Region;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RegisterParticlesModel::RegisterParticlesModel(G4String aModelName, G4Region* aEnvelope)
  : G4VFastSimulationModel(aModelName, aEnvelope)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RegisterParticlesModel::RegisterParticlesModel(G4String aModelName)
  : G4VFastSimulationModel(aModelName)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RegisterParticlesModel::~RegisterParticlesModel() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool RegisterParticlesModel::IsApplicable(const G4ParticleDefinition&)
{
  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool RegisterParticlesModel::ModelTrigger(const G4FastTrack&)
{
  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RegisterParticlesModel::DoIt(const G4FastTrack& aFastTrack, G4FastStep& aFastStep)
{
  // remove particle from further processing by G4
  aFastStep.KillPrimaryTrack();
  aFastStep.SetPrimaryTrackPathLength(0.0);
  G4double energy = aFastTrack.GetPrimaryTrack()->GetKineticEnergy();
  aFastStep.SetTotalEnergyDeposited(energy);
  G4ThreeVector position  = aFastTrack.GetPrimaryTrack()->GetPosition();
  G4double eta = position.eta();

  EventInformation* info = dynamic_cast<EventInformation*>(
    G4EventManager::GetEventManager()->GetConstCurrentEvent()->GetUserInformation());
  if(info == nullptr)
  {
    info = new EventInformation();
    G4EventManager::GetEventManager()->GetNonconstCurrentEvent()->SetUserInformation(info);
  }
  info->AddParticle(aFastTrack.GetPrimaryTrack()->GetParticleDefinition()->GetPDGEncoding(), energy, eta);
}
