#include "EventInformation.hh"
#include <CLHEP/Vector/ThreeVector.h>  // for operator<<
#include <G4VUserEventInformation.hh>  // for G4VUserEventInformation
#include <G4ios.hh>                    // for G4cout, G4endl
#include <ostream>                     // for operator<<, basic_ostream, ost...
#include "G4AnalysisManager.hh"          // for G4AnalysisManager
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventInformation::EventInformation()
  : G4VUserEventInformation()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventInformation::~EventInformation() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EventInformation::AddParticle(G4int aPID, G4double aEnergy, G4double aEta)
{
  // Get analysis manager
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  analysisManager->FillH2(0, aEnergy / GeV, aEta);
  if(aPID == 11 || aPID == -11) {
    analysisManager->FillH2(1, aEnergy / GeV, aEta);
    if(aEnergy > 100 * GeV)
      fElectrons[3] ++;
    if(aEnergy > 10 * GeV)
      fElectrons[2] ++;
    if(aEnergy > 1 * GeV)
      fElectrons[1] ++;
    if(aEnergy > 100 * MeV)
      fElectrons[0] ++;
  } else if(aPID == 22) {
    analysisManager->FillH2(2, aEnergy / GeV, aEta);
    if(aEnergy > 100 * GeV)
      fGammas[3] ++;
    if(aEnergy > 10 * GeV)
      fGammas[2] ++;
    if(aEnergy > 1 * GeV)
      fGammas[1] ++;
    if(aEnergy > 100 * MeV)
      fGammas[0] ++;
  } else if(aPID == 13 || aPID == -13) {
    analysisManager->FillH2(3, aEnergy / GeV, aEta);
    if(aEnergy > 100 * GeV)
      fMuons[3] ++;
    if(aEnergy > 10 * GeV)
      fMuons[2] ++;
    if(aEnergy > 1 * GeV)
      fMuons[1] ++;
    if(aEnergy > 100 * MeV)
      fMuons[0] ++;
  } else if(aPID == 111 || aPID == -111) {
    analysisManager->FillH2(4, aEnergy / GeV, aEta);
    if(aEnergy > 100 * GeV)
      fPi0s[3] ++;
    if(aEnergy > 10 * GeV)
      fPi0s[2] ++;
    if(aEnergy > 1 * GeV)
      fPi0s[1] ++;
    if(aEnergy > 100 * MeV)
      fPi0s[0] ++;
  } else if(aPID == 211 || aPID == -211) {
    analysisManager->FillH2(5, aEnergy / GeV, aEta);
    if(aEnergy > 100 * GeV)
      fChargedPions[3] ++;
    if(aEnergy > 10 * GeV)
      fChargedPions[2] ++;
    if(aEnergy > 1 * GeV)
      fChargedPions[1] ++;
    if(aEnergy > 100 * MeV)
      fChargedPions[0] ++;
  } else if(aPID == 2212 || aPID == -2212) {
    analysisManager->FillH2(6, aEnergy / GeV, aEta);
    if(aEnergy > 100 * GeV)
      fProtons[3] ++;
    if(aEnergy > 10 * GeV)
      fProtons[2] ++;
    if(aEnergy > 1 * GeV)
      fProtons[1] ++;
    if(aEnergy > 100 * MeV)
      fProtons[0] ++;
  } else if(aPID == 2112 || aPID == -2112) {
    analysisManager->FillH2(7, aEnergy / GeV, aEta);
    if(aEnergy > 100 * GeV)
      fNeutrons[3] ++;
    if(aEnergy > 10 * GeV)
      fNeutrons[2] ++;
    if(aEnergy > 1 * GeV)
      fNeutrons[1] ++;
    if(aEnergy > 100 * MeV)
      fNeutrons[0] ++;
  } else if(aPID == 12 || aPID == -12 || aPID == 14 || aPID == -14 || aPID == 16 || aPID == -16) {
    analysisManager->FillH2(8, aEnergy / GeV, aEta);
    if(aEnergy > 100 * GeV)
      fNeutrinos[3] ++;
    if(aEnergy > 10 * GeV)
      fNeutrinos[2] ++;
    if(aEnergy > 1 * GeV)
      fNeutrinos[1] ++;
    if(aEnergy > 100 * MeV)
      fNeutrinos[0] ++;
  } else {
    analysisManager->FillH2(9, aEnergy / GeV, aEta);
    if(aEnergy > 100 * GeV)
      fOthers[3] ++;
    if(aEnergy > 10 * GeV)
      fOthers[2] ++;
    if(aEnergy > 1 * GeV)
      fOthers[1] ++;
    if(aEnergy > 100 * MeV)
      fOthers[0] ++;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EventInformation::Print() const
{}

