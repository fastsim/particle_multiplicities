#include "DetectorConstruction.hh"
#include <G4Colour.hh>                     // for G4Colour
#include <G4Exception.hh>                  // for G4Exception
#include <G4ExceptionSeverity.hh>          // for FatalException
#include <G4SystemOfUnits.hh>              // for rad
#include <G4ThreeVector.hh>                // for G4ThreeVector
#include <G4VUserDetectorConstruction.hh>  // for G4VUserDetectorConstruction
#include <G4ios.hh>                        // for G4endl, G4cout
#include <algorithm>                       // for max
#include <numeric>                         // for accumulate
#include <ostream>                         // for operator<<, basic_ostream
#include <string>                          // for allocator, char_traits
#include "G4Box.hh"                        // for G4Box
#include "G4LogicalVolume.hh"              // for G4LogicalVolume
#include "G4Material.hh"                   // for G4Material
#include "G4NistManager.hh"                // for G4NistManager
#include "G4PVPlacement.hh"                // for G4PVPlacement
#include "G4Region.hh"                     // for G4Region
#include "G4RegionStore.hh"                // for G4RegionStore
#include "G4RunManager.hh"                 // for G4RunManager
#include "G4SDManager.hh"                  // for G4SDManager
#include "G4Tubs.hh"                       // for G4Tubs
#include "G4UnitsTable.hh"                 // for operator<<, G4BestUnit
#include "G4VisAttributes.hh"              // for G4VisAttributes
#include "RegisterParticlesModel.hh"
class G4VPhysicalVolume;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction(G4double aEta)
  : G4VUserDetectorConstruction()
  , fEta(aEta)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{
  //--------- Material definition ---------
  G4NistManager* nistManager = G4NistManager::Instance();
  G4Material* air            = nistManager->FindOrBuildMaterial("G4_AIR");

  //--------- Derived dimensions ---------
  G4double full2Pi = 2. * CLHEP::pi * rad;
  G4double layerThickness = 1 * CLHEP::mm;
  G4double detectorInnerRadius = 80 * CLHEP::cm;
  G4double detectorOuterRadius = detectorInnerRadius + layerThickness;
  G4double worldSizeXY         = detectorOuterRadius * 4.;
  G4double detectorLength = 2. * detectorInnerRadius / tan(2*atan(exp(-fEta)));
  G4double worldSizeZ          = detectorLength * 2;
  G4cout << "\n\n R =\t" << detectorInnerRadius << "\tL =\t" << detectorLength << "\teta =\t" << fEta << "\n\n" << G4endl;

  //--------- World ---------
  auto fSolidWorld  = new G4Box("World",                  // name
                                worldSizeXY / 2.,         // half-width in X
                                worldSizeXY / 2.,         // half-width in Y
                                worldSizeZ / 2.);         // half-width in Z
  auto fLogicWorld  = new G4LogicalVolume(fSolidWorld,    // solid
                                          air,            // material
                                          "World");       // name
  auto fPhysicWorld = new G4PVPlacement(0,                // no rotation
                                        G4ThreeVector(),  // at (0,0,0)
                                        fLogicWorld,      // logical volume
                                        "World",          // name
                                        0,                // mother volume
                                        false,            // not used
                                        99999,            // copy number
                                        false);           // check overlaps
  fLogicWorld->SetVisAttributes(G4VisAttributes::GetInvisible());

  //--------- Detector envelope ---------
  auto fSolidDetector = new G4Tubs("Detector",               // name
                                   detectorInnerRadius,     // inner radius
                                   detectorOuterRadius,      // outer radius
                                   detectorLength / 2.,     // half-width in Z
                                   0,                        // start angle
                                   full2Pi);                 // delta angle
  fLogicDetector = new G4LogicalVolume(fSolidDetector,  // solid
                                            air,             // material
                                            "Detector");     // name
  new G4PVPlacement(0,                                       // no rotation
                    G4ThreeVector(0, 0, 0),                  // detector centre at (0,0,0)
                    fLogicDetector,                          // logical volume
                    "Detector",                              // name
                    fLogicWorld,                             // mother volume
                    false,                                   // not used
                    9999,                                    // copy number
                    false);                                  // check overlaps

  // Region for fast simulation
  auto detectorRegion = new G4Region("DetectorRegion");
  detectorRegion->AddRootLogicalVolume(fLogicDetector);
  
  G4VisAttributes attribs;
  attribs.SetColour(G4Colour(0, 0, 1, 0.1));
  attribs.SetForceSolid(true);
  fLogicDetector->SetVisAttributes(attribs);

  return fPhysicWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::ConstructSDandField()
{
  auto detectorRegion = G4RegionStore::GetInstance()->GetRegion("DetectorRegion");
  new RegisterParticlesModel("registerModel", detectorRegion);
}
