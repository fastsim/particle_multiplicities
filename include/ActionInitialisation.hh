#ifndef ACTIONINITIALISATION_HH
#define ACTIONINITIALISATION_HH

#include "G4VUserActionInitialization.hh"

/**
 * @brief Initialization of user actions.
 *
 * Initialises the primary generator, and user actions (event, run) to perform
 * analysis and store histograms.
 *
 */

class ActionInitialisation : public G4VUserActionInitialization
{
 public:
  ActionInitialisation();
  ~ActionInitialisation();
  /// Create all user actions.
  virtual void Build() const final;
  /// Create run action in the master thread to allow analysis merging.
  virtual void BuildForMaster() const final;
};

#endif
