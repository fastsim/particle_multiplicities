#ifndef EVENTACTION_HH
#define EVENTACTION_HH

#include <G4Types.hh>            // for G4int, G4double
#include <vector>                // for vector
#include "G4UserEventAction.hh"  // for G4UserEventAction
class G4Event;

/**
 * @brief Event action class for particle analysis.
 *
 */

class EventAction : public G4UserEventAction
{
 public:
  EventAction();
  virtual ~EventAction();

  /// Timer is started
  virtual void BeginOfEventAction(const G4Event* aEvent) final;
  /// Hits collection is retrieved, analysed, and histograms are filled.
  virtual void EndOfEventAction(const G4Event* aEvent) final;
};

#endif /* EVENTACTION_HH */
