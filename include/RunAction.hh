#ifndef RUNACTION_HH
#define RUNACTION_HH

#include "G4UserRunAction.hh"
#include <CLHEP/Units/SystemOfUnits.h>       // for GeV
#include <G4String.hh>                       // for G4String
#include <G4ThreeVector.hh>                  // for G4ThreeVector
#include <G4Types.hh>                        // for G4int
class G4ParticleDefinition;

/**
 * @brief Run action
 *
 * Create analysis file and define control histograms.
 */

class RunAction : public G4UserRunAction
{
 public:
  /// Constructor. Defines the histograms.
  RunAction();
  virtual ~RunAction();

  /// Open the file for the analysis
  virtual void BeginOfRunAction(const G4Run*) final;
  /// Write and close the file
  virtual void EndOfRunAction(const G4Run*) final;

};

#endif /* RUNACTION_HH */
