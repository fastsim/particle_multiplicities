#ifndef HEPMC_G4_INTERFACE_H
#define HEPMC_G4_INTERFACE_H

#include "HepMC3/GenEvent.h"
#include <HepMC3/Units.h>
#include "G4Event.hh"
#include "G4SystemOfUnits.hh"

// We  have to take care for the position of primaries because
// primary vertices outside the world voulme give rise to G4Execption.
// If the default implementation is not adequate, an alternative
// can be implemented in your own class.
G4bool CheckVertexInsideWorld(const G4ThreeVector& aPos);

// service method for conversion from HepMC::GenEvent to G4Event
void HepMC2G4(const HepMC3::GenEvent* aHepMCEvent, G4Event* G4Event);

inline G4double HepMC2G4Length(G4double aValue, HepMC3::Units::LengthUnit aUnit) {
  HepMC3::Units::convert(aValue, aUnit, HepMC3::Units::MM);
  return aValue * mm;
};

inline G4double HepMC2G4Momentum(G4double aValue, HepMC3::Units::MomentumUnit aUnit) {
  HepMC3::Units::convert(aValue, aUnit, HepMC3::Units::MEV);
  return aValue * MeV;
};
#endif
