#ifndef REGISTERPARTICLESMODEL_HH
#define REGISTERPARTICLESMODEL_HH

#include <G4String.hh>                // for G4String
#include <G4Types.hh>                 // for G4bool
#include "G4VFastSimulationModel.hh"  // for G4VFastSimulationModel
class G4FastStep;
class G4FastTrack;
class G4ParticleDefinition;
class G4Region;

/**
 * @brief Register particles at calorimeter layer
 *
 */

class RegisterParticlesModel : public G4VFastSimulationModel
{
 public:
  RegisterParticlesModel(G4String, G4Region*);
  RegisterParticlesModel(G4String);
  ~RegisterParticlesModel();

  /// Check if particle is entering the volume. Check particle energy. It must be
  /// no smaller than 99% of the primary particle energy. This is to ensure that in case of
  /// prior interactions, particle energy does not differ (much) from the assumed
  ///  energy.
  virtual G4bool ModelTrigger(const G4FastTrack&) final;
  /// Model is applicable to all particles.
  virtual G4bool IsApplicable(const G4ParticleDefinition&) final;
  /// Check particle direction, entrance point, and store it in event information.
  /// Then go back to the full simulation.
  virtual void DoIt(const G4FastTrack&, G4FastStep&) final;
};
#endif /* REGISTERPARTICLESMODEL_HH */
