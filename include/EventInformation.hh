#ifndef EVENTINFORMATION_HH
#define EVENTINFORMATION_HH

#include <G4Types.hh>                    // for G4bool
#include "G4ThreeVector.hh"              // for G4ThreeVector
#include "G4VUserEventInformation.hh"    // for G4VUserEventInformation
#include <array>

/**
 * @brief Event information
 *
 */

class EventInformation : public G4VUserEventInformation
{
 public:
  EventInformation();
  virtual ~EventInformation();
  /*
  /// Set particle direction
  inline void SetDirection(const G4ThreeVector& aDirection) { fDirection = aDirection; };
  /// Get particle direction
  inline G4ThreeVector GetDirection() const { return fDirection; };
  /// Set particle position
  inline void SetPosition(const G4ThreeVector& aPosition) { fPosition = aPosition; };
  /// Get particle position
  inline G4ThreeVector GetPosition() const { return fPosition; };*/
  void AddParticle(G4int aPID, G4double aEnergy, G4double aEta);
  /// Print
  void Print() const final;
  inline G4int GetElectrons(G4int bin) const {return fElectrons[bin];};
  inline G4int GetGammas(G4int bin) const {return fGammas[bin];};
  inline G4int GetMuons(G4int bin) const {return fMuons[bin];};
  inline G4int GetPi0s(G4int bin) const {return fPi0s[bin];};
  inline G4int GetChargedPions(G4int bin) const {return fChargedPions[bin];};
  inline G4int GetProtons(G4int bin) const {return fProtons[bin];};
  inline G4int GetNeutrons(G4int bin) const {return fNeutrons[bin];};
  inline G4int GetNeutrinos(G4int bin) const {return fNeutrinos[bin];};
  inline G4int GetOthers(G4int bin) const {return fOthers[bin];};
  
  private:
  std::array<G4int,4> fElectrons {{0,0,0,0}};
  std::array<G4int,4> fGammas {{0,0,0,0}};
  std::array<G4int,4> fMuons {{0,0,0,0}};
  std::array<G4int,4> fPi0s {{0,0,0,0}};
  std::array<G4int,4> fChargedPions {{0,0,0,0}};
  std::array<G4int,4> fProtons {{0,0,0,0}};
  std::array<G4int,4> fNeutrons {{0,0,0,0}};
  std::array<G4int,4> fNeutrinos {{0,0,0,0}};
  std::array<G4int,4> fOthers {{0,0,0,0}};
  /*//// Particle direction. By default equal to the default particle gun direction.
  G4ThreeVector fDirection = { 0, 1, 0 };
  /// Particle position. By default equal to the default inner radius.
  G4ThreeVector fPosition = { 0, 800, 0 };
  /// Flag
  G4bool fIfSet = false;*/
};

#endif /* EVENTINFORMATION_HH */
