#ifndef DETECTORCONSTRUCTION_H
#define DETECTORCONSTRUCTION_H

#include <CLHEP/Units/SystemOfUnits.h>     // for cm, mm, pi, rad
#include <G4String.hh>                     // for G4String
#include <G4Types.hh>                      // for G4double, G4bool, G4int
#include <array>                           // for array
#include <cstddef>                         // for size_t
#include <vector>                          // for vector
#include "G4Material.hh"                   // for G4Material
#include "G4SystemOfUnits.hh"              // for cm, mm, rad
#include "G4ThreeVector.hh"                // for G4ThreeVector
#include "G4VUserDetectorConstruction.hh"  // for G4VUserDetectorConstruction
class G4LogicalVolume;
class G4VPhysicalVolume;

/**
 * @brief Detector construction.
 *
 * Creates a cylindrical detector, with cylinder axis along Z-axis. It is placed
 * in the  centre of the world volume.
 * 
 */

class DetectorConstruction : public G4VUserDetectorConstruction
{
 public:
  DetectorConstruction(G4double aEta);
  virtual ~DetectorConstruction();

  virtual G4VPhysicalVolume* Construct() final;
  virtual void ConstructSDandField() final;

 private:
  ///  Logical volume for parameterisation region
  G4LogicalVolume* fLogicDetector;
  G4double fEta = 1;
};

#endif /* DETECTORCONSTRUCTION_H */
