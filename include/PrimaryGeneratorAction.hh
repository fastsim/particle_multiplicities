#ifndef PRIMARYGENERATORACTION_HH
#define PRIMARYGENERATORACTION_HH

#include "G4VUserPrimaryGeneratorAction.hh"
#include "globals.hh"
#include <HepMC3/ReaderAscii.h>
#include <HepMC3/GenEvent.h>
class G4ParticleGun;

class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
public:
  PrimaryGeneratorAction();    
  virtual ~PrimaryGeneratorAction();

  void SetFileName(G4String aName);
  inline G4String GetFileName() const {return fFileName;};

  virtual void GeneratePrimaries(G4Event* event);
  
private:
  G4String fFileName = "gg2minbias.dat";//"gg2ttbar.dat";
  std::vector<std::unique_ptr<HepMC3::GenEvent>> events;
};

#endif
