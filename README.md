# particle_multiplicities

A simple Geant4 application that propagates particles and counts those passing certaing surface.

Counters check particles with different criterion on particle energy:

- E > 100 MeV
- E > 1 GeV
- E > 10 GeV
- E > 100 GeV

Different particle type:

- electrons and positrons
- gammas
- muons
- pions
- protons
- neutrons
- others
- all

And different eta range:

- |eta| < 1
- |eta| < 1.5
- |eta| < 3
- |eta| < 4
- |eta| < 6
- full eta coverage

## How to install it

Requirements:

- Geant4
- HepMC

```
mkdir build
cd build
cmake ..
make
```

## How to run it

```
./multiplicityCheck -eta 1 -m ttbar.mac
./multiplicityCheck -eta 4 -m minbias.mac
```
